#include<stdio.h>
#include<string.h>
#include<stdlib.h>
 
#define SZ 1000
 
int * get_substring_index(char *t, char *s);
int is_string_equal(char *t, char *s, int string_len); 
 
int main() {
  FILE *INFILE;
  INFILE = fopen("../DATA/rosalind_subs.txt", "r");
  char s[SZ], t[SZ];
  fscanf(INFILE, "%s\n%s", s, t);
 
  int *pos;
  pos = get_substring_index(t, s);
  int i=0;
  while(pos[i]) { 
    printf("%d\t", pos[i]);
    i++;
  }
  printf("\n");
  return 0;
}
 
int * get_substring_index(char *t, char *s) {
  // t is a substring of s
  int slen = strlen(s);
  int tlen = strlen(t);
 
  if (slen < tlen) 
    return NULL; 
 
  int *position = NULL;
  position = (int *) malloc(slen * sizeof(int));
  int i;
  int j = 0;
  for (i=0; i<=slen-tlen; i++) {
    if ( is_string_equal(t, s+i, tlen) ) {
      position[j++] = i + 1;
    }
  }
  position[j] = '\0';
  return position;
}
 
int is_string_equal(char *t, char *s, int string_len) {
  if (strlen(s) < string_len || strlen(t) < string_len) {
    return 0;
  } 
  int res = strncmp(t, s, string_len);
  if (res != 0)
    return 0;
  return 1;
}
