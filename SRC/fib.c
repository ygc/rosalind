#include<stdio.h>

#define MAXSIZE 41

unsigned long int fib(int n, int k);

int main() {
  int n, k;
  FILE *INFILE;
  INFILE = fopen("../DATA/rosalind_fib.txt", "r");
  fscanf(INFILE, "%d %d", &n, &k);
  printf("%lu\n", fib(n, k));
  return 0;
}

unsigned long int fib(int n, int k) {
  static unsigned long int cache[MAXSIZE] = {0,1,1,0,};
  if (cache[n] == 0)
    cache[n] = fib(n-1, k) + k * fib(n-2,k);
  return cache[n];
}
