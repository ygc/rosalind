#include<stdio.h>
#include<stdlib.h>

typedef struct ntNode {
  char NT; /* nucleotide */
  struct ntNode *next;
} ntNode;

int main() {
  FILE *INFILE;
  INFILE = fopen("../DATA/rosalind_revc.txt", "r");

  ntNode *head, *curr;
  head= NULL;

  char nt;
  while ( (nt = fgetc(INFILE)) != EOF) {
    curr = malloc(sizeof(ntNode));

    switch(nt) {
    case 'A':
      nt = 'T';
      break;
    case 'C':
      nt = 'G';
      break;
    case 'G':
      nt = 'C';
      break;
    case 'T':
      nt = 'A';
      break;
    default:
      nt = ' ';
    }

    curr->NT = nt;
    curr->next = head;
    head = curr;
  }

  curr = head;
  while(curr) {
    printf("%c", curr->NT);
    curr = curr->next;
  }
  printf("\n");
  return 0;
}
