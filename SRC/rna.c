#include<stdio.h>

int main() {
  FILE *INFILE;
  INFILE = fopen("../DATA/rosalind_rna.txt", "rt");
  char nt;
  while( (nt = fgetc(INFILE)) != EOF) {
    if (nt == 'T') {
      nt = 'U';
    }
    printf("%c", nt);
  }
  return 0;
}
