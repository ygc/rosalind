#include<stdio.h>

int main() {
  FILE *INFILE;
  INFILE=fopen("../DATA/rosalind_hamm.txt", "r");
  int length= 1000;
  char s1[length], s2[length];
  char nt;
  int flag = 1, i = 0, cnt=0;
  while( (nt = fgetc(INFILE)) != EOF) {
    if ( nt == '\n' ) { 
      length = i;
      flag = 2;
      i = 0;
      continue;
    }
    if (flag == 1) {
      s1[i++] = nt;
    } 
    if (flag == 2) {
      s2[i++] = nt;
    }
  }
  int j;
  for (j= 0; j < length; j++) {
    if (s1[j] != s2[j])
      cnt++;
  }
  
  printf("%d\n", cnt);
}
