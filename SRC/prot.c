#include <stdio.h>
#include <string.h>

typedef struct codon{
  char NA[3];  /* nucleic acid */
  char AA;     /* amino acid   */
} codon;

codon codonTable[64] = {
  {"UUU", 'F' }, {"CUU", 'L'}, {"AUU", 'I'}, {"GUU", 'V'}, 
  {"UUC", 'F' }, {"CUC", 'L'}, {"AUC", 'I'}, {"GUC", 'V'}, 
  {"UUA", 'L' }, {"CUA", 'L'}, {"AUA", 'I'}, {"GUA", 'V'}, 
  {"UUG", 'L' }, {"CUG", 'L'}, {"AUG", 'M'}, {"GUG", 'V'}, 
  {"UCU", 'S' }, {"CCU", 'P'}, {"ACU", 'T'}, {"GCU", 'A'}, 
  {"UCC", 'S' }, {"CCC", 'P'}, {"ACC", 'T'}, {"GCC", 'A'}, 
  {"UCA", 'S' }, {"CCA", 'P'}, {"ACA", 'T'}, {"GCA", 'A'}, 
  {"UCG", 'S' }, {"CCG", 'P'}, {"ACG", 'T'}, {"GCG", 'A'}, 
  {"UAU", 'Y' }, {"CAU", 'H'}, {"AAU", 'N'}, {"GAU", 'D'}, 
  {"UAC", 'Y' }, {"CAC", 'H'}, {"AAC", 'N'}, {"GAC", 'D'}, 
  {"UAA", '\0'}, {"CAA", 'Q'}, {"AAA", 'K'}, {"GAA", 'E'}, 
  {"UAG", '\0'}, {"CAG", 'Q'}, {"AAG", 'K'}, {"GAG", 'E'}, 
  {"UGU", 'C' }, {"CGU", 'R'}, {"AGU", 'S'}, {"GGU", 'G'}, 
  {"UGC", 'C' }, {"CGC", 'R'}, {"AGC", 'S'}, {"GGC", 'G'}, 
  {"UGA", '\0'}, {"CGA", 'R'}, {"AGA", 'R'}, {"GGA", 'G'}, 
  {"UGG", 'W' }, {"CGG", 'R'}, {"AGG", 'R'}, {"GGG", 'G'}
};

int main() {
  FILE *INFILE;
  INFILE=fopen("../DATA/rosalind_prot.txt", "r");
  int flag=0; /* bool variable, 0 for false, 1 for true */
  char code[3];
  
  while ( fgets(code, sizeof(code)+1, INFILE) != NULL ) {
    int i;
    for (i=0; i<64; i++) {
      if (strncmp(code, codonTable[i].NA,3) == 0) {
	if ( codonTable[i].AA == '\0') { /* stop codon */
	  flag = 1;
	  break;
	} else {
	  printf("%c", codonTable[i].AA);
	  break;
	}
      }
    }
    if (flag == 1) {
      break;
    }
  }
  printf("\n");
  return 0;
}
