#include<stdio.h>

int main() {
  FILE *INFILE;
  INFILE = fopen("../DATA/rosalind_dna.txt", "rt");
  char nt;
  int a_cnt, c_cnt, g_cnt, t_cnt; 
  a_cnt = c_cnt = g_cnt = t_cnt = 0;
  while( (nt = fgetc(INFILE)) != EOF) {
    switch(nt) {
    case 'A':
      a_cnt++;
      break;
    case 'C':
      c_cnt++;
      break;
    case 'G':
      g_cnt++;
      break;
    case 'T':
      t_cnt++;
      break;
    }
  }
  printf("%d %d %d %d\n", a_cnt, c_cnt, g_cnt, t_cnt);
  return 0;
}
