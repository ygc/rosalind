#include<stdio.h>

int main() {
  FILE *INFILE;
  INFILE = fopen("../DATA/rosalind_gc.txt", "r");
  char ch;
  int desc_size = 13;
  char desc[desc_size], max_gc_desc[desc_size];
  int gc_cnt=0, seq_length=0;
  double gc,  max_gc=0.0;
  int desc_line = 0, i;
  while( (ch = fgetc(INFILE) ) != EOF ) {
    // description line
    if ( ch == '>' || desc_line == 1) {
      i=0;
      if ( ch != '>' ) {
	desc[i++] = ch;
      }
      while( ( ch = fgetc(INFILE) ) != '\n' ) {
	desc[i++] = ch;
      }
    }

    // sequence lines
    desc_line = 0;
    while ( (ch = fgetc(INFILE)) != '>' && ch != EOF) {
      if ( ch == '\n' ) {
	continue;
      }
      seq_length++;
      if (ch == 'G' || ch == 'C') {
	gc_cnt++;
      }
    }

    if (seq_length != 0) {
      gc = (double) gc_cnt*100 / seq_length;
      if (gc > max_gc) {
	max_gc = gc;
	for (i=0; i < desc_size; i++) {
	  max_gc_desc[i] = desc[i];
	}
      }
    }

    gc_cnt = 0;
    seq_length = 0;
    desc_line = 1;

  }

  for (i=0; i < desc_size; i++) {
    printf("%c", max_gc_desc[i]);
  }
  printf("\n%8.6f\n", max_gc);
  return 0;
}


